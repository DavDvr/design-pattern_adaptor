<?php

namespace DPAdapter\App;

interface Standard
{
    public function operation(int $nb1, int $nb2): void;

}