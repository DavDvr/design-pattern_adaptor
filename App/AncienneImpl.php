<?php

namespace DPAdapter\App;

class AncienneImpl
{
    public function calcul(float $nb1, int $nb2): float
    {
        return 2 * $nb1 + $nb2;
    }

    public function dispayNbr(float $nb)
    {
        echo "_______Ancienne Implémentation_____<br>";
        echo "le nombre est: $nb";
    }
}