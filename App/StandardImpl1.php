<?php

namespace DPAdapter\App;

class StandardImpl1 implements Standard
{

    public function operation(int $nb1, int $nb2): void
    {
        $result = $nb1 * $nb2;
        echo '________Implémentation standard 1___<br>';
        echo "result = $result <br>";
    }
}