<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home Adapter</title>
</head>
<body>

<?php

use DPAdapter\App\Adapter;
use DPAdapter\App\Client;
use DPAdapter\App\StandardImpl1;
use DPAdapter\App\StandardImpl2;

require_once (dirname(__DIR__)."/vendor/autoload.php");


//implementation standard qui fait le traitement et affiche le resultat
$client = new Client();
$client->setStandard(new StandardImpl1());
$client->process(40, 12);
$client->setStandard(new StandardImpl2());
$client->process(10, 12);

//implementation avec adapter
$client->setStandard(new Adapter());
$client->process(10,10);

?>

</body>
</html>