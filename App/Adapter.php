<?php

namespace DPAdapter\App;

class Adapter extends AncienneImpl implements Standard
{
    //soit extends AncienneImpl soit Implement Standard


   // private AncienneImpl $ancienneImpl;

    /**
     * Adapter constructor.
     */
    public function __construct()
    {
       // $this->ancienneImpl = new AncienneImpl();
    }


    public function operation(int $nb1, int $nb2): void
    {
        $result = $this->calcul((float)$nb1, $nb2);
        $this->dispayNbr($result);
    }
}