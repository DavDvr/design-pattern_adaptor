<?php

namespace DPAdapter\App;

class StandardImpl2 implements Standard
{

    public function operation(int $nb1, int $nb2): void
    {
        $result = $nb1 * $nb2;
        echo '_______Implémentation standard 2___<br>';
        echo "result = $result <br>";
    }
}