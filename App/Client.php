<?php

namespace DPAdapter\App;

class Client
{
    private Standard $standard;

    public function process(int $nb1, int $nb2)
    {
        $this->standard->operation($nb1, $nb2);
    }

    /**
     * @return Standard
     */
    public function getStandard(): Standard
    {
        return $this->standard;
    }

    /**
     * @param Standard $standard
     */
    public function setStandard(Standard $standard): void
    {
        $this->standard = $standard;
    }

}